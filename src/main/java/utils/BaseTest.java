package utils;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import org.testng.annotations.Listeners;

@Listeners(ExtentITestListenerClassAdapter.class)
public class BaseTest {

    ObterProperties prop = new ObterProperties();

    @BeforeClass()
    public void baseSetUp() {
        Web.loadPage(prop.get("baseUri"));
    }

    @AfterClass
    public void tearDown(){Web.close();}
}
