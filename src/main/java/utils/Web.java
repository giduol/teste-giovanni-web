package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Web {

    public static WebDriverWait wait;
    public static WebDriver navegador;

    public static WebDriver getCurrentDriver() {
        if(navegador == null) {

            //DESCOMENTAR PAR RODAR NO DRIVER LOCAL
            /*System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver.exe");
            navegador = new ChromeDriver();*/

            //COMENTAR PARA RODAR NO DRIVER LOCAL
            WebDriverManager.chromedriver().setup();
            ChromeOptions opt = new ChromeOptions();
            opt.setHeadless(true);
            navegador = new ChromeDriver(opt);


            wait = new WebDriverWait(navegador, 10);
            navegador.manage().window().maximize();
            navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

        return navegador;
    }
    public static void close(){
        getCurrentDriver().quit();
        navegador = null;
    }
    public static void loadPage(String url){
        getCurrentDriver().get(url);
    }
}