package mapearElementos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComprarPeloCarrinhoElementos {

    @FindBy(id = "user-name")
    public WebElement user;

    @FindBy(id = "password")
    public WebElement password;

    @FindBy(id = "login-button")
    public WebElement botaoLogin;

    @FindBy(id = "add-to-cart-sauce-labs-backpack")
    public WebElement mochila;

    @FindBy(id = "add-to-cart-test.allthethings()-t-shirt-(red)")
    public WebElement camiseta;

    @FindBy(id = "shopping_cart_container")
    public WebElement carrinho;

    @FindBy(id = "checkout")
    public WebElement checkout;

    @FindBy(id = "first-name")
    public WebElement firstName;

    @FindBy(id = "last-name")
    public WebElement lastName;

    @FindBy(id = "postal-code")
    public WebElement postalCode;

    @FindBy(id = "continue")
    public WebElement botaoContinue;

    @FindBy(id = "finish")
    public WebElement botaoFinish;

    @FindBy(xpath = "//*[@id=\"checkout_complete_container\"]/h2")
    public WebElement mensagemFinal;
}
