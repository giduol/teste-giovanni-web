package testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.UsuarioBloqueadoPage;
import utils.BaseTest;

public class UsuarioBloqueado extends BaseTest {

    @Test()
    public void loginUsuarioBloqueado() {
        UsuarioBloqueadoPage compra = new UsuarioBloqueadoPage();
        compra.incluirnome("locked_out_user");
        compra.incluirsenha("secret_sauce");
        compra.clicarLogin();
        Assert.assertEquals(compra.mensagemErro(), "Epic sadface: Sorry, this user has been locked out.");
        System.out.println("Mensagem de erro: " + compra.mensagemErro());
    }
}







