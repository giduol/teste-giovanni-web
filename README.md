**Desafio WEB Ailos**

Testes automatizados disponibilizados pela url: https://www.saucedemo.com.

**Objetivo:**

Validar os seguintes casos de testes:

• Login com usuário bloqueado, e validar a mensagem de bloqueio exibida;<br>
• Realizar uma compra completa com mais de um produto no carrinho;<br>
• Adicionar todos os produtos ao carrinho e validar o valor total da compra a ser pago.<br>

**Recursos utilizados:**

• Java 11; <br>
• Selenium; <br>
• Google Chrome; <br> 
• Maven; <br>
• Testng. <br>

**Como configurar o ambiente:**

• Faça clone do projeto: https://gitlab.com/giduol/teste-giovanni-ailos-web.git; <br>
• Importe o projeto para sua IDE de preferência;<br>
• Necessário ter instalado o JDK11. <br>

**Como executar a aplicação:**

• src/test/java/testcases: acessar para rodar os cenários de testes.<br>

**Relatórios:**

A aplicação gera os relatórios automaticamente e pode ser visualizado no arquivo html: relatorio\execucao.html

**Pipeline:**

Os testes estão rodando na pipeline do Gitlab CI/CD conforme configurado no gitlab-ci.yml.
